<?php
//user.php

include('database_connection.php');

if(!isset($_SESSION["type"]))
{
	header('location:login.php');
}

if($_SESSION["type"] != 'master')
{
	header("location:index.php");
}

include('header.php');


?>

<style>

	.radio-space{
		padding-left:0px;
	}
</style>

		<span id="alert_action"></span>
		<div class="row">
			<div class="col-lg-12">
				<div class="panel panel-default">
                    <div class="panel-heading">
                    	<div class="row">
                        	<div class="col-lg-10 col-md-10 col-sm-8 col-xs-6">
                            	<h3 class="panel-title">User List</h3>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-6" align="right">
                            	<button type="button" name="add" id="add_button" data-toggle="modal" data-target="#userModal" class="btn btn-success btn-xs">Add</button>
                        	</div>
                        </div>
                       
                        <div class="clear:both"></div>
                   	</div>
                   	<div class="panel-body">
                   		<div class="row"><div class="col-sm-12 table-responsive">
                   			<table id="user_data" class="table table-bordered table-striped">
                   				<thead>
									<tr>
										<th>ID</th>
										<th>Mobile</th>
										<th>Name</th>
										<th>Status</th>
										<th>Edit</th>
										<th>Delete</th>
									</tr>
								</thead>
                   			</table>
                   		</div>
                   	</div>
               	</div>
           	</div>
        </div>
        <div id="userModal" class="modal fade">
        	<div class="modal-dialog">
        		<form method="post" id="user_form">
        			<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title"><i class="fa fa-plus"></i> Add User</h4>
        			</div>
        			<div class="modal-body">
						<div class="form-group">
							<label>User Name</label>
							<input type="text" name="user_id" id="user_id" class="form-control" readonly />
						</div>
						<div class="form-group">
							<label>Password</label>
							<input type="password" name="user_password" id="user_password" class="form-control" required />
						</div> 
						<div class="form-group">
							<label>Email</label>
							<input type="text" name="user_email" id="user_email" class="form-control" required />
						</div>
						<div class="form-group">
							<label>Name</label>
							<input type="text" name="name" id="name" class="form-control" required />
						</div>
						<div class="form-group">
							<label>Mobile:</label>
							<input type="text" name="mobile" id="mobile" class="form-control" required />
						</div>
						 <div class="form-group">
							<label>Date:</label>
							<input type="date" name="date" id="date" class="form-control" required />
						</div> 
						<div class="form-group">
							<label>Building</label>
							<input type="text" name="Building" id="Building" class="form-control" required />
						</div>
						<div class="form-group">
							<label>Batch</label>
							<input type="number" name="Batch" id="Batch" class="form-control" required />
						</div>
						<div class="form-group">
							<label>No of Servings</label>
							<input type="number" name="Servings" id="Servings" class="form-control" required />
						</div>
						<div class="form-group">
							<label for="tiffin_size"> Tiffin Size</label></br>
							<select name="tiffin_size" id="tiffin_size" class="form-control">
    							<option value="Small">Small</option>
    							<option value="Medium">Medium</option>
    							<option value="Large">Large</option>
  							</select>
						</div>
						<div class="form-group">
							<label>Applied Servings</label>
							<input type="number" name="Appl_Serving" id="Appl_Serving" class="form-control" required />
						</div>
						<div class="form-group">
							<label>Thaali</label>
							<input type="text" name="Thaali" id="Thaali" class="form-control" required />
						</div>
						<div class="form-group">
							<div class="radio-space">
							<label style = padding-left : 0px>Active:  </label>
							<input type="radio" id="yes" name="active" value="yes" style = padding-left : 35px checked>
							<label for="yes" >YES  </label>
							<input type="radio" id="no" name="active" value="no" style = padding-left : 35px>
							<label for="no">NO  </label></div>
						</div>
        			</div>
        			<div class="modal-footer">
        				<input type="hidden" name="user_id" id="user_id" />
        				<input type="hidden" name="btn_action" id="btn_action" />
        				<input type="submit" name="action" id="action" class="btn btn-info" value="Add" />
        				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        			</div>
        		</div>
        		</form>

        	</div>
        </div>
<script>
$(document).ready(function(){

	$('#add_button').click(function(){
		$('#user_form')[0].reset();
		$('.modal-title').html("<i class='fa fa-plus'></i> Add User");
		$('#action').val("Add");
		$('#btn_action').val("Add");
	});

	var userdataTable = $('#user_data').DataTable({
		"processing": true,
		"serverSide": true,
		"order": [],
		"ajax":{
			url:"user_fetch.php",
			type:"POST"
		},
		"columnDefs":[
			{
				"target":[4,5],
				"orderable":false
			}
		],
		"pageLength": 25
	});

	$(document).on('submit', '#user_form', function(event){
		event.preventDefault();
		$('#action').attr('disabled','disabled');
		var form_data = $(this).serialize();
		$.ajax({
			url:"user_action.php",
			method:"POST",
			data:form_data,
			success:function(data)
			{
				$('#user_form')[0].reset();
				$('#userModal').modal('hide');
				$('#alert_action').fadeIn().html('<div class="alert alert-success">'+data+'</div>');
				$('#action').attr('disabled', false);
				userdataTable.ajax.reload();
			}
		})
	});

	$(document).on('click', '.update', function(){
		var user_id = $(this).attr("id");
		var btn_action = 'fetch_single';
		$.ajax({
			url:"user_action.php",
			method:"POST",
			data:{user_id:user_id, btn_action:btn_action},
			dataType:"json",
			success:function(data)
			{
				$('#userModal').modal('show');
				$('#user_name').val(data.user_name);
				$('#user_email').val(data.user_email);
				$('.modal-title').html("<i class='fa fa-pencil-square-o'></i> Edit User");
				$('#user_id').val(user_id);
				$('#name').val(data.name);
				$('#mobile').val(data.mobile);
				$('#date').val(data.date);
				$('#Building').val(data.Building);
				$('#Batch').val(data.Batch);
				$('#Servings').val(data.Servings);
				$('#tiffin_size').val(data.tiffin_size);
				$('#Appl_Serving').val(data.Appl_Serving);
				$('#Thaali').val(data.Thaali);
				$('#active').val(data.active);
				$('#action').val('Edit');
				$('#btn_action').val('Edit');
				$('#user_password').attr('required', false);
			}
		})
	});

	$(document).on('click', '.delete', function(){
		var user_id = $(this).attr("id");
		var status = $(this).data('status');
		var btn_action = "delete";
		if(confirm("Are you sure you want to change status?"))
		{
			$.ajax({
				url:"user_action.php",
				method:"POST",
				data:{user_id:user_id, status:status, btn_action:btn_action},
				success:function(data)
				{
					$('#alert_action').fadeIn().html('<div class="alert alert-info">'+data+'</div>');
					userdataTable.ajax.reload();
				}
			})
		}
		else
		{
			return false;
		}
	});

});
</script>

<?php
include('footer.php');
?>
