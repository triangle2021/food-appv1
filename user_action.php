<?php

//user_action.php

include('database_connection.php');

if(isset($_POST['btn_action']))
{
	if($_POST['btn_action'] == 'Add')
	{
		$query = "
		INSERT INTO  thaali_users (user_password,user_email,user_type, user_status, name, mobile, Building, Date, Batch, Servings, Tiffin_Size,Appl_Serving,Thaali,Active) 
		VALUES (:user_password, :user_email, :user_type, :user_status , :name, :mobile, :Building, :date, :Batch, :Servings, :tiffin_size, :Appl_Serving, :Thaali, :active)
		";	
		$statement = $connect->prepare($query);
		$statement->execute(
			array(
				':user_password'	=> password_hash($_POST["user_password"], PASSWORD_DEFAULT),
				':user_email'		=>	$_POST["user_email"],
				':user_type'		=>	'user',
				':user_status'		=>	'active',
				':name'				=>	$_POST["name"],
				':mobile'			=>	$_POST["mobile"],
				':Building'			=>	$_POST["Building"],
				':date'				=>	$_POST["date"],
				':Batch'			=>	$_POST["Batch"],
				':Servings'			=>	$_POST["Servings"],
				':tiffin_size'		=>	$_POST["tiffin_size"],
				':Appl_Serving'		=>	$_POST["Appl_Serving"],
				':Thaali'			=>	$_POST["Thaali"],
				':active'			=>	$_POST["active"]
			)
		);
		$result = $statement->fetchAll();
		if(isset($result))
		{
			echo 'New User Added';
		}
	}
	if($_POST['btn_action'] == 'fetch_single')
	{
		$query = "
		SELECT * FROM  thaali_users WHERE user_id = :user_id
		";
		$statement = $connect->prepare($query);
		$statement->execute(
			array(
				':user_id'	=>	$_POST["user_id"],
			)
		);
		$result = $statement->fetchAll();
		foreach($result as $row)
		{
			$output['user_email'] = $row['user_email'];
			$output['user_name'] = $row['user_name'];
			$output['name'] = $row['Name'];
			$output['mobile'] = $row['mobile'];
			$output['Building'] = $row['Building'];
			$output['date'] = $row['Date'];
			$output['Batch'] = $row['Batch'];
			$output['Servings'] = $row['Servings'];
			$output['tiffin_size'] = $row['Tiffin_Size'];
			$output['Appl_Serving'] = $row['Appl_Serving'];
			$output['Thaali'] = $row['Thaali'];
			$output['active'] = $row['Active'];
		}
		echo json_encode($output);
	}
	if($_POST['btn_action'] == 'Edit')
	{
		if($_POST['user_password'] != '')
		{
			$query = "
			UPDATE user_details SET 
			name = '".$_POST["name"]."',
			mobile = '".$_POST["mobile"]."', 
			Building = '".$_POST["Building"]."', 
			date = '".$_POST["date"]."',
			Batch = '".$_POST["Batch"]."',
			Servings = '".$_POST["Servings"]."',
			tiffin_size = '".$_POST["tiffin_size"]."',
			Appl_Serving = '".$_POST["Appl_Serving"]."',
			Thaali = '".$_POST["Thaali"]."',
			active = '".$_POST["active"]."',
				user_password = '".password_hash($_POST["user_password"], PASSWORD_DEFAULT)."' 
				WHERE user_id = '".$_POST["user_id"]."'
			";
		}
		else
		{
			$query = "
			UPDATE thaali_users SET 
				name = '".$_POST["name"]."',
				mobile = '".$_POST["mobile"]."', 
				Building = '".$_POST["Building"]."', 
				date = '".$_POST["date"]."',
				Batch = '".$_POST["Batch"]."',
				Servings = '".$_POST["Servings"]."',
				tiffin_size = '".$_POST["tiffin_size"]."',
				Appl_Serving = '".$_POST["Appl_Serving"]."',
				Thaali = '".$_POST["Thaali"]."',
				active = '".$_POST["active"]."'
				WHERE user_id = '".$_POST["user_id"]."'
			";
		}
		$statement = $connect->prepare($query);
		$statement->execute();
		$result = $statement->fetchAll();
		if(isset($result))
		{
			echo 'User Details Edited';
		}
	}
	if($_POST['btn_action'] == 'delete')
	{
		$status = 'Active';
		if($_POST['status'] == 'Active')
		{
			$status = 'Inactive';
		}
		$query = "
		UPDATE thaali_users 
		SET user_status = :user_status 
		WHERE user_id = :user_id
		";
		$statement = $connect->prepare($query);
		$statement->execute(
			array(
				':user_status'	=>	$status,
				':user_id'		=>	$_POST["user_id"]
			)
		);	
		$result = $statement->fetchAll();	
		if(isset($result))
		{
			echo 'User Status change to ' . $status;
		}
	}
}

?>